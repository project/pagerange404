<?php

namespace Drupal\pagerange404\EventSubscriber;

use Drupal;
use Drupal\views\Views;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Page Range 404 Subscriber
 *
 * Returns a 404 instead of the last available page when a Views pagination
 * request is out of range.
 *
 * @package Drupal\pagerange404\EventSubscriber
 */
class PageRange404Subscriber implements EventSubscriberInterface {

  /**
   * The request stack
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public RequestStack $requestStack;
  private Drupal\Core\Path\PathValidator $pathValidator;

  /**
   * Constructs a PageRange404Subscriber object.
   */
  public function __construct(RequestStack $requestStack, Drupal\Core\Path\PathValidator $path_validator) {
    $this->requestStack = $requestStack;
    $this->pathValidator = $path_validator;
  }

  /**
   * Kernel request event handler.
   */
  public function onKernelRequest(RequestEvent $event): void {

    if ($event->isMainRequest()) {
      $request = $this->requestStack->getCurrentRequest();
      $query = $request->query->all();

      $page = isset($query['page']) ? $query['page'] : NULL;

      if (!empty($page)) {
        if (is_string($page)) {
          $path = $request->getPathInfo();

          // Get info about the URL path
          if ($url = $this->pathValidator->getUrlIfValid($path)) {
            // Get route parameters.
            $params = $url->getRouteParameters();

            // If there's a 'view_id' parameter, load the view.
            if (isset($params['view_id'])) {
              // Load the view by view ID and display ID.
              $view = Views::getView($params['view_id']);
              $args = [];

              // Any param not view_id or display_id assumed to be an argument
              foreach ($params as $key => $value) {
                if (!in_array($key, ['view_id', 'display_id'])) {
                  $args[] = $value;
                }
              }

              if (!empty($params['display_id'])) {
                $view->setDisplay($params['display_id']);
              }

              $view->setArguments($args);
              $view->execute();
              $current_page = $view->getCurrentPage();

              if ($page != $current_page) {
                throw new NotFoundHttpException();
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => ['onKernelRequest', 10],
    ];
  }

}
