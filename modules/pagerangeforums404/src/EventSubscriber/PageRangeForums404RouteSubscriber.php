<?php

namespace Drupal\pagerangeforums404\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber to override default forumPage method
 */
class PageRangeForums404RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('forum.page')) {
      $route->setDefault('_controller', '\Drupal\pagerangeforums404\Controller\PageRangeForums404Controller::forumPage');
    }
  }
}
