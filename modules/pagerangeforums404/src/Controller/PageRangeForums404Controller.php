<?php

namespace Drupal\pagerangeforums404\Controller;

use Drupal\forum\Controller\ForumController;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Page Range Forums 404 routes.
 */
class PageRangeForums404Controller extends ForumController {
  /**
   * Extended version of forumPage method
   */
  public function forumPage(TermInterface $taxonomy_term) {

    $request = \Drupal::service('request_stack')->getCurrentRequest();
    $tid     = $taxonomy_term->id();
    $page    = $request->query->get('page');

    if (!empty($page)) {
      $config   = \Drupal::service('config.factory')->get('forum.settings');
      $interval = $config->get('topics.page_limit');

      $db    = \Drupal::service('database');
      $query = $db->select('forum', 'f');
      $query->addField('f', 'nid');
      $query->groupBy('f.nid');
      $query->condition('f.tid', $tid);

      $user = \Drupal::currentUser();
      if ($user->hasPermission('administer forums') === FALSE) {
        $query->leftJoin('node_field_data', 'n', 'f.nid = n.nid');
        $query->condition('n.status', 1);
      }

      $results = $query->execute()->fetchAll();
      $total   = count($results);

      $max_pages = floor($total / $interval);

      // Silly bots, we don't accept negative page numbers.
      if ($page < 0 || $page > $max_pages) {
        throw new NotFoundHttpException();
      }
    }
    return parent::forumPage($taxonomy_term);
  }

}
