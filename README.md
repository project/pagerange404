## INTRODUCTION

The normal Drupal behavior on paginated displays when a request is made for an
invalid page number is to serve the last available page to the user. If someone
requests page=5 and there are only 4 pages, page 4 gets served. This can be
undesirable if you have aggressive bots crawling your site or you've recently
changed your pagination setup and you don't want to keep getting traffic to
invalid URLs.

Page Range 404 changes default Drupal behavior for paginated Views displays to
serve a 404 response instead of the last page in the index when a request is
received for an out of range page number. It includes a submodule for serving
404 responses on forum index pages served by the Forum module in Drupal 10 core.

## CONFIGURATION
There is nothing to configure. Once you install it, Page Range 404 will evaluate
any access to a Views path that has a "page" query variable to check if that
page exists before rendering the page.

If you enable the optional Page Range Forums 404 module, the same behavior will
be applied to your Forum index pages.

## USAGE

On a high traffic site that is getting heavily crawled by bots, this module can
paradoxically drive up your server load if you're serving fully rendered 404
pages. Depending on your setup, a Drupal 404 page can actually be more expensive
than the cached Views response that it's replacing.

For this reason, you may also want to install something like Fast 404 to reduce
the impact on your server.

## MAINTAINERS

Current maintainers for Drupal 10:

- Sheldon Chang (hyperlinked) - https://www.drupal.org/u/hyperlinked
